#!/bin/sh

set -e

version="$1"

OSNAME=apertis
NAME=Apertis
HOME_URL=https://www.apertis.org/
BUG_REPORT_URL=https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues
PARENT=Debian

cat > etc/issue << EOF
$NAME $version \\n \\l
EOF

cat > etc/issue.net << EOF
$NAME $version
EOF

cat > etc/os-release << EOF
PRETTY_NAME="$NAME $version"
NAME="$NAME"
VERSION_ID="$version"
VERSION="$version"
VERSION_CODENAME="$version"
ID=$OSNAME
ID_LIKE=debian
HOME_URL="$HOME_URL"
SUPPORT_URL="$BUG_REPORT_URL"
BUG_REPORT_URL="$BUG_REPORT_URL"
EOF

cat > origins/$OSNAME << EOF
Vendor: $NAME
Vendor-URL: $HOME_URL
Bugs: $BUG_REPORT_URL
Parent: $PARENT
EOF

sed "s/VENDORFILE = .*/VENDORFILE = $OSNAME/" -i debian/rules

git diff --exit-code && exit 0

pkg_ver=$(dpkg-parsechangelog -S Version)

dch_arg=-l+$OSNAME

case "$pkg_ver" in
    *~v*+*)
        ;;
    *~v*)
        dch_arg=-v"$pkg_ver"+1
        ;;
esac

dch $dch_arg -D $OSNAME --force-distribution "Update base files for $OSNAME/$version"

dpkg-buildpackage -S -d --no-pre-clean -uc -us
